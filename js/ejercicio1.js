const calcular = document.getElementById('calcular');
calcular.addEventListener('click', function () {
    let precio = document.getElementById('precio').value;
    let tipoViaje = document.getElementById('tipoViaje').value;

    let subtotal = document.getElementById('subtotal').value;
    let impuesto = document.getElementById('impuesto').value;
    let total = document.getElementById('total').value;

    let subtotalvr = 0;
    let impuestovr = 0;
    let totalvr = 0;

    if (tipoViaje == 1) {
        subtotalvr = precio;
    }
    else {
        subtotalvr = precio * 1.8;
    }
    impuestovr = subtotalvr * 0.16;
    totalvr = subtotalvr + impuestovr;

    document.getElementById('subtotal').value = subtotalvr;
    document.getElementById('impuesto').value = impuestovr;
    document.getElementById('total').value = totalvr;
});

const limpiar = document.getElementById('limpiar');
limpiar.addEventListener('clicl', function () {

    document.getElementById('numBoleto').value = "";
    document.getElementById('numCliente').value = "";
    document.getElementById('destino').value = "";
    document.getElementById('tipoViaje').selectedIndex = "";
    document.getElementById('preico').value = "";
    document.getElementById('subtotal').value = "";
    document.getElementById('impuesto').value = "";
    document.getElementById('total').value = "";

});